(defproject web-app "1.0"
  :description "Simple Web App"
  :url "http://ravegrunt.com"
  :min-lein-version "2.0.0"
  :license {:name "EPL-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                     [ring/ring "1.8.2"]
                     [org.clojure/tools.logging "1.1.0"]]
  :main web-app.core
  :profiles {:uberjar {:aot :all :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}
            :cicd {:local-repo ".m2/repository"}})
