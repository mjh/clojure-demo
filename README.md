# Demo Web App

A demo web app for testing Waypoint with Clojure

## Background

[HashiCorp's Waypoint](https://www.waypointproject.io/) is a new tool announced at HashiCon 2020 that automates many steps of a deployment process.
This new tool is mostly tied around 3 main, declared functions - build, deploy, and release.
This focus allows it to simply all the mechanics behind the scenes, such as building the image with [buildpacks](https://buildpacks.io/), creating an entry point, requesting a load balancer, and providing an easy to use URL on the LetsEncrypt secure `waypoint.run` domain.

[Clojure](https://clojure.org/) is a functional, general purpose language leveraging lisp-like syntax compiled on the JVM.
It's a fun, elegent, and powerful language - if you have any curiousity to it, I'd recommend Daniel Higginbotham's famous [Clojure for the Brave and True](https://www.braveclojure.com/).

However, unlike most demos, Clojure requires compiling into a jar and deploying similar to a java applciation, which means that it, in turn, doesn't flow as well as the nodejs and go apps in the [Waypoint examples repository](https://github.com/hashicorp/waypoint-examples)

This leverages the Paketo build pack and a Procfile to otherwise do what is mighty tricky otherwise!


## Installation

Clone this repo, connect a Kubernetes config, a registry, and a waypoint server.

Then just hit `waypoint up`

## What next?

 You can go to your Waypoint.run url afor a simple "Hello World" or any path for "Hello <path>!" with something such as `<yourdeployment>-waypoint.run/boudicca` displaying "Hello Boudicca!"

## Waypoint HCL

The HCL assumes a few things:

  * A *Project* called "Waypoint-Demos"
  * An *App* called "clj-demo"
  * An image registry (currently set to my personal one; `harbor.ravegrunt.com`)
  * A Kubernetes cluster with a *namespace* of `waypoint-clj`
  * You want to use the [Paketo Buildpacks](https://paketo.io/)

You can change these to fit your environment - a point of note is that if you use the normal Heroku or GCP Builders, you will likely need ot change the `service_port` in deploy to 3000, as this is Heroku (but not Paketo's) default.

## gitlab-ci

### Personal Choices

My setup involves:

 * A single $5 droplet k8s on DigitalOcean
 * A self-hosted [Harbor registry](https://goharbor.io/)

If you don't want to use either of these: 

For registry:

  * You can log into any registry you choose, such as Docker Hub or Artifactory among many, many others.
  * If you want to use GitLab's registry simply editing the `docker login` line to `docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY`.

For Kubernetes:

  * You can provide a kubeconfig file, and export it as a variable.
  * You can provide an AWS key pair, however there is [more involved setup](https://docs.gitlab.com/ee/user/project/clusters/add_eks_clusters.html)
  * GKE is also [an option involving more setup](https://about.gitlab.com/blog/2020/03/27/gitlab-ci-on-google-kubernetes-engine/), but GitLab does have an agreement to give you more free Google credits for trying their GKE integration


### Variables

Please store all the below as environment variables, except possibly `WAYPOINT_SERVER_TLS` and `WAYPOINT_SERVER_TLS_SKIP_VERIFY`, as these don't yet have an override available from Waypoint (yet).

| Variable                        | Default | What you need                                                                   |
|---------------------------------|---------|---------------------------------------------------------------------------------|
| WAYPOINT_SERVER_ADDR            | ''      | Address of your Waypoint API endpoint (usually on port 9701) - can be URL or IP |
| WAYPOINT_SERVER_TOKEN           | ''      | Token from running `waypoint token new`                                         |
| WAYPOINT_SERVER_TLS             | 1       | Enables TLS to the server, default                                              |
| WAYPOINT_SERVER_TLS_SKIP_VERIFY | 1       | Ignores the fact the servers have self-signed certs                             |
| DO_API_KEY                      | ''      | DigitalOcean API key that has access to a pre-create Kubernetes cluster         |
| DO_K8S_NAME                     | ''      | DigitalOcean cluster name                                                       |
| REGISTRY_URL                    | ''      | URL of the registry where you will push and pull your built image               |
| REGISTRY_USER                   | ''      | Username to log into aforementioned registry                                    |
| REGISTRY_PASSWORD               | ''      | Password to authenticate into registry with                                     |

### How it works

This will:

  * Use the official image of Waypoint 0.1.5
  * Log into the provided registry
  * Get and install DigitalOcean's [doctl tool](https://github.com/digitalocean/doctl) version 1.42
  * Authenticate into the provided DO account
  * Grab the Kubeconfig for the provided cluster
  * Waypoint init, build, deploy, and release!

## Future iterations

  * Echo back or somehow provide the current release URL
  * Housekeeping/cleanup/destroy functions

## License

Copyright © 2020 Marty Henderson

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
