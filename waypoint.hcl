project = "waypoint-demos"

runner {
  enabled = true
    data_source "git" {
    url  = "https://gitlab.com/mjh/clojure-demo.git"
    }
}

app "clj-demo" {
  labels = {
    "service" = "clj-demo",
    "env"     = "dev"
  }

  build {
    use "pack" {
    builder="paketobuildpacks/builder:base"
    }
    registry {
      use "docker" {
        image = "registry.digitalocean.com/nalthis/waypoint-clojure"
        tag  = "latest"
      }
    }
  }

  deploy {
    use "kubernetes" {
      probe_path = "/"
      namespace = "waypoint-clj"
      service_port = 8080
    }
  }

  release {
    use "kubernetes" {
      load_balancer = true
      port          = 8080
      namespace = "waypoint-clj"
    }
  }
}